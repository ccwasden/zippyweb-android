package acr.browser.lightning;

import android.app.Application;
import android.content.Context;

import com.spritzinc.android.sdk.SpritzSDK;

public class BrowserApp extends Application {

	private static Context context;

	@Override
	public void onCreate() {
		super.onCreate();
        SpritzSDK.init(this,
                "4e6b1475a6d9199e7",
                "10a417b0-fc14-4e41-ac4e-fcdc6fadd57c",
                "https://sdk.spritzinc.com/android/examples/login_success.html"
        );
		context = getApplicationContext();
	}

	public static Context getAppContext() {
		return context;
	}
}
