package acr.browser.lightning;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Picture;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.*;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.WindowManager.LayoutParams;
import android.widget.RelativeLayout;
import android.util.TypedValue;
import android.app.Dialog;
import android.widget.SeekBar;
import android.widget.SeekBar.*;
import android.os.Handler;
import android.widget.TextView;

import com.spritzinc.android.SimpleSpritzSource;
import com.spritzinc.android.UrlSpritzSource;
import com.spritzinc.android.sdk.SpritzSDK;
import com.spritzinc.android.sdk.SpritzUser;
import com.spritzinc.android.sdk.SpritzViewListener;
import com.spritzinc.android.sdk.view.SpritzBaseView;
import com.spritzinc.android.sdk.view.SpritzFullControlView;

import com.loopj.android.http.*;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import android.webkit.WebView.PictureListener;

import acr.browser.lightning.R;

public class SpritzActivity extends Activity implements SpritzSDK.LoginEventListener,
        SpritzSDK.LoginStatusChangeListener  {

    private SpritzBaseView spritzView;
    private String url;
    private String content;
    private WebView _webView;
    private static final String PREF_SPEED = "speed";
    static String TAG = "CCW";
    private SharedPreferences sharedPrefs;
    private static final String SHARED_PREFS_NAME = "main";

    @Override
    public void onUserLoginStatusChanged(boolean b) {
        updateLoggedInUser();
    }

    @Override
    public void onLoginStart() {
        Log.i("H", "C");
    }

    @Override
    public void onLoginSuccess(){
        Log.i("H","D");
    }

    @Override
    public void onLoginFail(java.lang.String s, java.lang.Throwable throwable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Login Failed");

        StringBuilder message = new StringBuilder();
        message.append(s);

        if (throwable != null) {
            message.append(": ");
            message.append(throwable.getMessage());
        }

        builder.setMessage(message.toString());
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Ignore
            }
        });
        builder.show();
    }

    @Override
    protected void onPause() {
        // Pause the spritz view.
        spritzView.pause();

        SpritzSDK.getInstance().removeLoginStatusChangeListener(this);
        SpritzSDK.getInstance().removeLoginEventListener(this);

        Log.i("H","A");
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadSpritz();
            }
        }, 150);
    }

    @Override
    protected void onResume() {
        Log.i("H","B");
        super.onResume();
        SpritzSDK.getInstance().addLoginEventListener(this);
        SpritzSDK.getInstance().addLoginStatusChangeListener(this);

        updateLoggedInUser();


    }


    private void updateLoggedInUser() {

//        SpritzUser user = SpritzSDK.getInstance().getLoggedInUser();
//
//        Button btnLogin = (Button)findViewById(R.id.btnLogin);
//        TextView tvLoggedInUser = (TextView)findViewById(R.id.tvLoggedInUser);
//
//        if (user == null) {
//
//            tvLoggedInUser.setText(getResources().getString(R.string.activity_main_none));
//            btnLogin.setText(getResources().getString(R.string.activity_main_login));
//
//        } else {
//
//            StringBuilder userInfo = new StringBuilder();
//            userInfo.append(user.getUserId());
//
//            if (user.getNickname() != null) {
//                userInfo.append(" (");
//                userInfo.append(user.getNickname());
//                userInfo.append(")");
//            }
//
//            tvLoggedInUser.setText(userInfo.toString());
//            btnLogin.setText(getResources().getString(R.string.activity_main_logout));
//        }
    }


    private SharedPreferences getSharedPrefs() {
        if (sharedPrefs == null) {
            sharedPrefs = getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        }

        return sharedPrefs;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getWindow().requestFeature(android.view.Window.FEATURE_ACTION_BAR);
//        getActionBar().hide();

        final SharedPreferences prefs = getSharedPrefs();

        setContentView(R.layout.activity_spritz);

        setSpritzSize(retrieveSavedSpritzSize());
        getActionBar().setDisplayHomeAsUpEnabled(true);
        spritzView = (SpritzBaseView)findViewById(R.id.spritzView);

        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        if(url != null) {
            url = url.replace("http://mobile.", "http://www.");
            url = url.replace("https://mobile.", "https://www.");
            url = url.replace("http://m.", "http://www.");
            url = url.replace("https://m.", "https://www.");

            longInfo(url);

            this.url = url;
        }


        String content = intent.getStringExtra("text");
        this.content = content;


        if (savedInstanceState == null) {
            // Restore saved speed, if any, on initial creation.  If this is a re-create because of an orientation
            // change, skip this step, because the spritzView will remember the selected speed.
            int savedSpeed = prefs.getInt(PREF_SPEED, -1);

            if (savedSpeed != -1) {
                spritzView.setSpeed(savedSpeed);
            }
        }


        String title = intent.getStringExtra("title");
        setTitle(title.length() > 0 ? title : url);



//        ProgressDialog progress = new ProgressDialog(this);
////        progress.setMessage("Downloading Music :) ");
//        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progress.setIndeterminate(true);
//        progress.show();





    }

    private void showLoading(final boolean loading){
        runOnUiThread(new Runnable() {
            public void run() {
                progressLayout().setVisibility(loading ? View.VISIBLE : View.GONE);
            }
        });

    }

    private View progressLayout(){
        return (RelativeLayout)findViewById(R.id.progress_layout);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.spritz, menu);
        return true;
    }

    private int dpToPixels(int dp){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    private void setSpritzSize(int size){
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,dpToPixels(size));
        RelativeLayout layout1 = (RelativeLayout) findViewById(R.id.spritzLayout);
        layout1.setLayoutParams(params);
        layout1.invalidate();
    }

    private int retrieveSavedSpritzSize(){
        SharedPreferences preferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        int size = preferences.getInt("wasd_spritz_size", 250);
        return size;
    }

    private void saveSpritzSize(int size){
        SharedPreferences preferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("wasd_spritz_size", size);
        editor.apply();
    }


    private void loadUrlSource(){
        runOnUiThread(new Runnable() {
            public void run() {
                UrlSpritzSource source = new UrlSpritzSource(url);
                spritzView.load(source);
                showLoading(false);
            }
        });
    }

    private void loadTextSource(String text){
        final String txt = text;
        runOnUiThread(new Runnable() {
            public void run() {
                SimpleSpritzSource source = new SimpleSpritzSource(txt,null);
//                new Locale("en", "US"));
                spritzView.load(source);
                showLoading(false);
            }
        });

    }

    private class JavscriptHandler {
        @JavascriptInterface
        public void processHTML(String text) {
            if (null != text && text.trim().length() > 0) {

                loadTextSource(text);
            }
            else {

                loadUrlSource();
            }
        }
    }

    public static void longInfo(String str) {
        if(str.length() > 4000) {
            Log.i(TAG, str.substring(0, 4000));
            longInfo(str.substring(4000));
        } else
            Log.i(TAG, str);
    }



    private void loadSpritz(){

        if(content != null && content.length() > 0){
            loadTextSource(content);
        }
        else if(url != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            Map<String,String> map = new HashMap<String,String>();
            map.put("token","a8fe74655ab8e2d8dfcfd7627fdf43356c9fe56e"); // "d30903c47cd5205e66337c79e0d9fbd3aa9bcc3a");
            map.put("url",url);

            final SpritzActivity actv = this;
            RequestParams params = new RequestParams(map);
            client.get("https://www.readability.com/api/content/v1/parser", params, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    _webView = new WebView(actv);
//                RelativeLayout layout = (RelativeLayout)findViewById(R.id.spritzLayout);
//                layout.addView(_webView);

                    try {
                        String html = (String)response.get("content");

                        longInfo(html);
//                    webView.loadUrl(url);

                        String fullHtml = "<html><head></head><body><div>"+html+"</div></body></html>";
                        String jsHtml = "'"+fullHtml
                                .replaceAll("\"","\\\\\"")
                                .replaceAll("\n","")
                                .replaceAll("\r","")
                                .replaceAll("'","\\\\'")+"'";

                        final String jsCommand = "document.innerHTML = "+jsHtml;

                        _webView.getSettings().setJavaScriptEnabled(true);
                        _webView.addJavascriptInterface(new JavscriptHandler(),"HTMLOUT");
                        _webView.loadData("", "text/html", null);
                        _webView.loadUrl("javascript:"+jsCommand);
                        _webView.loadUrl("javascript:Array.prototype.slice.call( document.getElementsByTagName('pre'), 0).forEach(function(e){"+
                                "var newElem = document.createElement('DIV');"+
                                "newElem.innerText = '<<_CODE_BLOCK_>>';"+
                                "e.parentElement.replaceChild(newElem,e);"+
                                "});");
                        _webView.loadUrl("javascript:HTMLOUT.processHTML(document.documentElement.innerText);");

                    }
                    catch (Exception e){
                        Log.e("ERR",e.getMessage());
                        loadUrlSource();

                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Log.i("ZIPPY.ERR",responseString);
                    loadUrlSource();
                }

            });
        }


    }

    @Override
    protected void onDestroy() {
        SharedPreferences prefs = getSharedPrefs();

        if (spritzView.getSpeed() != prefs.getInt(PREF_SPEED, -1)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(PREF_SPEED, spritzView.getSpeed());
            editor.commit();
        }

        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id){
        case R.id.action_spritz_size:

            FragmentManager manager = getFragmentManager();
            SpritzSDK.getInstance().loginUser(manager);

//            Dialog dialog = new Dialog(this);
//                dialog.setTitle("Spritz Size");
//            LayoutInflater inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
//            View layout = inflater.inflate(R.layout.slider_dialog, (ViewGroup)findViewById(R.id.slider_dialog_main));
//            dialog.setContentView(layout);
//            WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
//            wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
//            wmlp.y = 100;   //y position
//
//            SeekBar seekBar = (SeekBar)layout.findViewById(R.id.your_dialog_seekbar);
//            final int rangeMin = 160;
//            int rangeMax = 500;
//            seekBar.setMax(rangeMax-rangeMin);
//            seekBar.setKeyProgressIncrement(1);
//            seekBar.setProgress(retrieveSavedSpritzSize()-rangeMin);
//            OnSeekBarChangeListener yourSeekBarListener = new OnSeekBarChangeListener() {
//                @Override
//                public void onStopTrackingTouch(SeekBar seekBar) {
//                    saveSpritzSize(seekBar.getProgress()+rangeMin);
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar seekBar) {}
//
//                @Override
//                public void onProgressChanged(SeekBar seekBark, int progress, boolean fromUser) {
//                    setSpritzSize(progress+rangeMin);
//                }
//            };
//
//            seekBar.setOnSeekBarChangeListener(yourSeekBarListener);
//                dialog.show();


            return true;

        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
