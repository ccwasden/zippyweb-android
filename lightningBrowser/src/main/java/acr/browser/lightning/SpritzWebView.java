package acr.browser.lightning;

import android.app.Activity;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

/**
 * Created by chasewasden on 8/1/14.
 */
public class SpritzWebView extends WebView {

    public SpritzWebView(Activity actv){
        super(actv);
    }



    public ActionMode startActionMode(final ActionMode.Callback callback)
    {
        ActionMode.Callback newCallback = new ActionMode.Callback(){

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu){
                menu.add(Menu.FIRST,232,Menu.FIRST,"Spritz").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                boolean res = callback.onCreateActionMode(mode, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu){
                return callback.onPrepareActionMode(mode, menu);
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if(item.getItemId() == 232){
                    loadUrl("javascript:$HTML_OUT$.useText(window.getSelection().toString());");
//                    Log.i("CCW","SPRITZ!");
                    return false;
                }
                return callback.onActionItemClicked(mode, item);
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                callback.onDestroyActionMode(mode);
            }
        };
        return super.startActionMode(newCallback);
    }


}
